from rest_framework import serializers
from .models import Product, Tag


class ProductSerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(source='category.name')
    tags = serializers.SlugRelatedField(many=True, slug_field='name', queryset=Tag.objects.all())

    class Meta:
        model = Product
        fields = ('id', 'name', 'description', 'price', 'category_name', 'created_at', 'tags')
