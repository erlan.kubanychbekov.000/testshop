from django.urls import path
from .views import ExportProductAndListView

urlpatterns = [
    path('products/', ExportProductAndListView.as_view({'get': 'list'}), name='list_product'),
    path('products/export/', ExportProductAndListView.as_view({'get': 'export_excel'}), name='export_excel')
]
