from base_mixins.viewset_mixins import CachedListAndExportExcelViewSet
from .serializers import ProductSerializer
from .models import Product


class ExportProductAndListView(CachedListAndExportExcelViewSet):
    """
    View is returns list of products and export excel file
    """
    title = 'Products'
    queryset = Product.objects.select_related('category').prefetch_related('tags')
    serializer_class = ProductSerializer
    columns = ['ID', 'Name', 'Description', 'Price', 'Category']
    file_name = 'products'
