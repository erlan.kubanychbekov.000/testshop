FROM python:3
ENV PYTHONUNBUFFERED 1
WORKDIR /test_shop

ADD . /test_shop
RUN pip install -r requirements.txt