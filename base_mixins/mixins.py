from openpyxl import Workbook
from django.http import HttpResponse


class ExportExcelMixin:
    """
    Export File mixin, can export any model.
    Inherit this class from viewsets to work properly
    """
    title = None
    queryset = None
    columns = None
    file_name = title

    def make_excel(self):
        # Create exel file and configurate fields and columns
        workbook = Workbook()
        worksheet = workbook.active
        worksheet.title = self.title
        for col_num, column_title in enumerate(self.columns, 1):
            cell = worksheet.cell(row=1, column=col_num)
            cell.value = column_title

        for row_num, model in enumerate(self.queryset, 2):
            for col_num, field in enumerate(self.columns, 1):
                value = getattr(model, field.lower())
                worksheet.cell(row=row_num, column=col_num, value=str(value))
        return workbook

    def export_excel(self, request):
        # export excel logic
        excel_file = self.make_excel()
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = f'attachment; filename={self.file_name.lower()}.xlsx'
        excel_file.save(response)
        return response
