from django.core.cache import cache
from rest_framework.response import Response
from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet
from base_mixins.mixins import ExportExcelMixin


class OnlyListViewSet(ListModelMixin, GenericViewSet):
    pass


class CachedListAndExportExcelViewSet(OnlyListViewSet, ExportExcelMixin):
    # cached list
    def list(self, request, *args, **kwargs):
        cache_key = 'object_list'
        cached_data = cache.get(cache_key)

        if cached_data is None:
            queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(queryset, many=True)
            cache.set(cache_key, serializer.data)
            return Response(serializer.data)
        else:
            return Response(cached_data)
