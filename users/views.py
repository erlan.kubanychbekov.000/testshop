from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework import status
from rest_framework.response import Response

from rest_framework_simplejwt.views import TokenObtainPairView

from .serializers import RegisterUserSerializer


class UserRegisterAPIView(APIView):
    """
    User register view,
    """
    serializer_class = RegisterUserSerializer
    permission_classes = [AllowAny]

    def post(self, request):
        serializer = self.serializer_class(data=self.request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'message': 'User registered successfully.'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CustomTokenObtainView(TokenObtainPairView):
    """
    Custom view for get jwt token and change 'access' and 'refresh' fields to 'token' ,'refresh_token'
    """
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        token = serializer.validated_data['access']
        refresh_token = serializer.validated_data['refresh']
        return Response({'token': token, 'refresh_token': refresh_token})
