from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    """
    Custom user via AbstractUser, first_name, first_name fields removed
    """
    username = models.CharField(max_length=50, unique=True)
    email = models.EmailField(unique=True)
    first_name = None
    last_name = None

    def __str__(self):
        return self.email
