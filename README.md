## TestShop

### Start project

To run the project, you need **Python** and **Docker Compose** installed.
Create a `.env` file and fill it with the following example content:
```text
SECRET_KEY='django-insecure-5#&g(sp-6r5@g82_*ojkx(82jzpswkvt1ef9lw37%gdj@^d3$j'
REDIS_LOCATION='redis://redis:6379/'
```
Next, launch Docker using the command:
```bash
 docker-compose up
```

### Creating a Superuser

Create a superuser to access the admin panel by executing the following commands:

``` bash
docker-compose exec geo sh
```

Enter the command:

```bash
python manage.py createsuperuser
```
#### Shop will be available here  http://0.0.0.0:8000/
#### To view the documentation go to http://127.0.0.1:8000/api/swagger/


#### To authorize a user, you need a `JWT`
First, register here http://0.0.0.0:8000/api/register/
Then get the token http://0.0.0.0:8000/api/login/
The received token must be sent along with requests to `header` 


### Optimization 

When querying for the list of products, the **select_related** and **prefetch_related** methods were applied. These methods enable performing joins and reducing the number of database queries, which contributes to lowering the load. Additionally, to prevent frequent and redundant database queries, query caching was implemented using Redis.